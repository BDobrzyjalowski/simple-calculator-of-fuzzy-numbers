import java.awt.List;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Collections;
import sun.nio.cs.ArrayDecoder;

/**
 * 
 */

/**
 * @author Bartek
 *
 */
class element
{
	int value;
	double probability;

	public element(int value, double probability)
	{
		this.value=value;
		this.probability=probability;
	}
	
	public element()
	{
		this.value=-1;
		this.probability=-1;
	}
	
	public int getValue()
	{
		return value;
	}
	
	public double getProbability()
	{
		return probability;
	}
	
	public String toString()
	{
		return value + " " + probability;
	}
}


class collection
{
	ArrayList<element> col = new ArrayList<>();
	
	public void addElement(element x)
	{
		col.add(x);
	}
	
	public Boolean validationCollection()
	{
		int count=0;
		Boolean second_stage=false;
		int tmp_value=0;
		double tmp_probability=0;
		int licznik=0;
		for (element obj : col)
		{
			if(obj.getProbability()<0 || obj.getProbability()>1)
			{
				System.out.println("Error one");
				return false;
			}
		}
		
		for (element obj : col)
		{
			if(obj.getProbability()!=1)
			{
				count++;
			}
		}
		if(count==col.size())
		{
			System.out.println("Error two");
			return false;
		}
		
		for (element obj : col)
		{
			if(licznik!=0)
			{
				if(tmp_value>=obj.getValue())
				{
					System.out.println("Error three");
					return false;
				}
			}
			tmp_value=obj.getValue();
			licznik++;
		}
		licznik=0;
		
		for (element obj : col)
		{
			if(tmp_probability==1)
			{
				second_stage=true;
			}
			if(licznik==0)
			{
				tmp_probability=obj.getProbability();
			}			
			else if(tmp_probability>1 || second_stage==true)
			{
				if(tmp_probability<obj.getProbability())
				{
					
					System.out.println("Error five.2");
					return false;
				}
			}
			else if(tmp_probability<1 )
			{
				if(tmp_probability>obj.getProbability())
				{
					
					System.out.println("Error five.1" + tmp_probability + " " + obj.getProbability());
					return false;
				}
			}
			
			tmp_probability=obj.getProbability();
			licznik++;
		}
		return true;		
	}
	
}

class comparatorProbabilityOfElements implements Comparator<element>{ 
    @Override
    public int compare(element o1, element o2) { 
            if (o1.getProbability() > o2.getProbability()) { 
            return -1; 
    } 
            else if (o1.getProbability() < o2.getProbability()) { 
           return 1; 
    } 
        return 0; 
}
}

class comparatorValuesOfElements implements Comparator<element>{ 
    @Override
    public int compare(element o1, element o2) { 
            if (o1.getValue() > o2.getValue()) { 
            return 1; 
    } 
            else if (o1.getValue() < o2.getValue()) { 
           return -1; 
    } 
        return 0; 
}
}

public class Calculator {

	/**
	 * @param args
	 */
	public static int generate_substraction_of_values(element one, element two)
	{
		return one.getValue()-two.getValue();
	}
	

	
	public static int generate_amount_of_values(element one, element two)
	{
		return one.getValue()+two.getValue();
	}
	public static double generate_probabilities(element one, element two)
	{
		return (one.getProbability()+two.getProbability())/2;
	}
	
	public static collection Substract(collection one, collection two)
	{
		collection result = new collection();
		collection new_result = new collection();
		for (element first : one.col)
		{
			for (element second : two.col)
			{
				element x = new element(generate_substraction_of_values(first, second), generate_probabilities(first, second));
				result.col.add(x);
			}
		}
		while(result.col.size()>0)
		{
			Collections.sort(result.col, new comparatorProbabilityOfElements()); 
			element start = result.col.get(0);
			new_result.col.add(start);
			int flag=0;
			do
			{
				element t=new element();
				flag=0;
				for (element test : result.col)
				{
					if(test.getValue()==start.getValue())
					{
						flag++;
						t=test;
						break;
					}
				}
				result.col.remove(t);
				
			}while(flag!=0);
		}
		return new_result;
	}
	
	public static collection Adder(collection one, collection two)
	{
		collection result = new collection();
		collection new_result = new collection();
		for (element first : one.col)
		{
			for (element second : two.col)
			{
				element x = new element(generate_amount_of_values(first, second), generate_probabilities(first, second));
				result.col.add(x);
			}
		}
		while(result.col.size()>0)
		{
			Collections.sort(result.col, new comparatorProbabilityOfElements()); 
			element start = result.col.get(0);
			new_result.col.add(start);
			int flag=0;
			do
			{
				element t=new element();
				flag=0;
				for (element test : result.col)
				{
					if(test.getValue()==start.getValue())
					{
						flag++;
						t=test;
						break;
					}
				}
				result.col.remove(t);
				
			}while(flag!=0);
		}
		return new_result;
	}
	public static void main(String[] args) {
		element a1 = new element(3, 0);
		element a2 = new element(4, 0.4);
		element a3 = new element(5, 1);
		element a4 = new element(6, 0.6);
		element a5 = new element(7, 0.2);
		element a6 = new element(8, 0);
		
		element b1 = new element(-3, 0);
		element b2 = new element(-2, 0.5);
		element b3 = new element(-1, 0.8);
		element b4 = new element(0, 1);
		element b5 = new element(1, 0.3);
		element b6 = new element(2, 0);
		
		collection number_one = new collection();
		collection number_two = new collection();
		number_one.col.add(a1);
		number_one.col.add(a2);
		number_one.col.add(a3);
		number_one.col.add(a4);
		number_one.col.add(a5);
		number_one.col.add(a6);
		
		number_two.col.add(b1);
		number_two.col.add(b2);
		number_two.col.add(b3);
		number_two.col.add(b4);
		number_two.col.add(b5);
		number_two.col.add(b6);
		
		
		System.out.println(number_one.validationCollection());
		System.out.println(number_two.validationCollection());
		
		collection result = Adder(number_one, number_two);
		Collections.sort(result.col, new comparatorProbabilityOfElements()); 

		for (element second : result.col)
		{
			System.out.println(second.toString());
		}
		System.out.println("ODEJMWOANIE");
		result = Substract(number_one, number_two);
		Collections.sort(result.col, new comparatorValuesOfElements()); 

		for (element second : result.col)
		{
			System.out.println(second.toString());
		}
		
		
	}

}
